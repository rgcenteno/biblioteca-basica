/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionbiblioteca;

import org.daw1.gestionbiblioteca.clases.*;
/**
 *
 * @author rgcenteno
 */
public class GestionBiblioteca {

    private static Biblioteca biblioteca;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        biblioteca = new Biblioteca("DAW1", "As Neves - Tortoreos");
        System.out.println("Biblioteca dada de alta \n" + biblioteca); 
        biblioteca.addLibro(new Libro("123-12-12345-12-1", "Libro de prueba", "Autor de prueba", 2018, Genero.valueOf("HISTORICA"), "Mi editorial", 100, 2));
        biblioteca.addLibro(new Libro("123-12-12345-13-1", "Libro de prueba 3", "Autor de prueba 3", 2019, Genero.of(1), "Mi editorial", 300, 2));
        
        System.out.println("\nTodos los libros");        
        for(Libro l : biblioteca.getLibros()){
            System.out.println(l);
        }
        
        System.out.println("\nMostrar solo novelas históricas");
        System.out.println("En stock: " + biblioteca.countLibrosGenero(Genero.valueOf("HISTORICA")));
        for(Libro l : biblioteca.getLibros(Genero.valueOf("HISTORICA"))){
            System.out.println(l + "\n");
        }
        
    }
    
}
