/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionbiblioteca.clases;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
/**
 * Implementación con Set para almacenar los libros. Se implementa para mostrar otra posible solución. Se puede probar en main cambiando las cadenas Biblioteca 
 * @author rgcenteno
 */
public class BibliotecaSet{
    private final String nombre;
    private final String direccion;
    private final Set<Libro> libros;

    public BibliotecaSet(String nombre, String direccion) {
        checkNotNullBlank(nombre);
        checkNotNullBlank(direccion);
        this.nombre = nombre;
        this.direccion = direccion;
        this.libros = new TreeSet<>();        
    }

    public BibliotecaSet(String nombre) {
        checkNotNullBlank(nombre);
        this.nombre = nombre;
        this.direccion = "SD";
        this.libros = new TreeSet<>();
    }
    
    public boolean addLibro(Libro l){        
        return this.libros.add(l);
    }
    
    public boolean removeLibro(Libro l){
        return this.libros.remove(l);
    }
    
    private static void checkNotNullBlank(String txt){
        java.util.Objects.requireNonNull(txt);
        if(txt.isBlank()){
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return this.nombre + " --- " + this.direccion;
    }
    
    /**
     * Obtiene todos los libros de la biblioteca
     * @return 
     */
    public Set<Libro> getLibros(){
        return java.util.Collections.unmodifiableSet(libros);
    }
    
    public Set<Libro> getLibros(Genero g){
        java.util.Set<Libro> resultado = new java.util.HashSet<>();
        for(Libro l : this.libros){
            if(l.getGenero().equals(g)){
                resultado.add(l);
            }
        }
        return resultado;    
    }
    
    public int countLibrosGenero(Genero g){
        int resultado = 0;
        for(Libro l : this.libros){
            if(l.getGenero().equals(g)){
                resultado++;
            }
        }
        return resultado; 
    }
    
    public boolean borrarLibroByIsbn(String isbn){
        Iterator<Libro> it = libros.iterator();
        while (it.hasNext()) {
            Libro next = it.next();
            if(next.getIsbn().equals(isbn)){
                it.remove();
                return true;
            }
        }
        return false;
    }
}
