/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionbiblioteca.clases;

/**
 *
 * @author rgcenteno
 */
public class Libro implements Comparable<Libro>{
    private final String isbn;
    private final String titulo;
    private final String nombreAutor;
    private final int anhoPublicacion;
    private final Genero genero;
    private final String editorial;
    private final int stock;
    private int edicion;

    public Libro(String isbn, String titulo, String nombreAutor, int anhoPublicacion, Genero genero, String editorial, int stock) {
        checkIsbn(isbn);
        checkNotNullBlank(titulo);
        checkNotNullBlank(nombreAutor);
        checkNotNullBlank(editorial);
        checkOneOrGreater(stock);
        checkAnhoPublicacion(anhoPublicacion);
        checkOneOrGreater(edicion);
        java.util.Objects.requireNonNull(genero);
        
        this.isbn = isbn;
        this.titulo = titulo;
        this.nombreAutor = nombreAutor;
        this.anhoPublicacion = anhoPublicacion;
        this.genero = genero;
        this.editorial = editorial;
        this.stock = stock;
        this.edicion = 1;
    }

    public Libro(String isbn, String titulo, String nombreAutor, int anhoPublicacion, Genero genero, String editorial, int stock, int edicion) {
        checkIsbn(isbn);
        checkNotNullBlank(titulo);
        checkNotNullBlank(nombreAutor);
        checkNotNullBlank(editorial);
        checkAnhoPublicacion(anhoPublicacion);
        checkOneOrGreater(stock);
        checkOneOrGreater(edicion);
        java.util.Objects.requireNonNull(genero);
        
        this.isbn = isbn;
        this.titulo = titulo;
        this.nombreAutor = nombreAutor;
        this.anhoPublicacion = anhoPublicacion;
        this.genero = genero;
        this.editorial = editorial;
        this.stock = stock;
        this.edicion = edicion;
    }
    
    private static void checkIsbn(String isbn){
        java.util.Objects.requireNonNull(isbn);
        java.util.regex.Pattern patron = java.util.regex.Pattern.compile("[0-9]{3}\\-[0-9]{2}\\-[0-9]{5}\\-[0-9]{2}\\-[0-9]");
        if(!patron.matcher(isbn).matches()){
            throw new IllegalArgumentException("El ISBN debe seguir el formato 123-12-12345-12-1");
        }
    }
    
    private static void checkNotNullBlank(String txt){
        java.util.Objects.requireNonNull(txt);
        if(txt.isBlank()){
            throw new IllegalArgumentException();
        }
    }
    
    private static void checkOneOrGreater(int num){
        if(num < 1){
            throw new IllegalArgumentException();
        }
    }
    
    private static void checkAnhoPublicacion(int year){
        if(year > java.time.LocalDate.now().getYear()){
            throw new IllegalArgumentException();
        }
    }

    public void setEdicion(int edicion) {
        this.edicion = edicion;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else {
            if (obj instanceof Libro) {
                Libro aux = (Libro) obj;
                return aux.isbn.equals(this.isbn);
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(isbn);
    }

    @Override
    public String toString() {
        return "ISBN: " + this.isbn + "\n" + this.titulo + "\n" + this.editorial + "\n" + this.edicion + "\n" + this.stock;
    }

    public int getEdicion() {
        return edicion;
    }

    public Genero getGenero() {
        return genero;
    }

    public String getIsbn() {
        return isbn;
    }
    
    

    @Override
    public int compareTo(Libro t) {
        java.util.Objects.requireNonNull(t);
        return this.isbn.compareTo(t.isbn);
    }
    
    
}
