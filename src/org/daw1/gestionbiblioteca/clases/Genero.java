/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionbiblioteca.clases;

/**
 *
 * @author rgcenteno
 */
public enum Genero {
    DRAMA, COMEDIA, POLICIACA, HISTORICA, ENSAYO, TEATRO;
        
    public static Genero of(int eleccion) {
        java.util.Objects.checkIndex(eleccion - 1, Genero.values().length);
        return Genero.values()[eleccion - 1];
    }
        
}
