/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionbiblioteca.clases;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;
/**
 *
 * @author rgcenteno
 */
public class Biblioteca {
    private final String nombre;
    private final String direccion;
    private final Map<Genero, Set<Libro>> libros;

    public Biblioteca(String nombre, String direccion) {
        checkNotNullBlank(nombre);
        checkNotNullBlank(direccion);
        this.nombre = nombre;
        this.direccion = direccion;
        this.libros = new HashMap<>();        
    }

    public Biblioteca(String nombre) {
        checkNotNullBlank(nombre);
        this.nombre = nombre;
        this.direccion = "SD";
        this.libros = new HashMap<>();
    }
    
    public boolean addLibro(Libro l){
        if(!libros.containsKey(l.getGenero())){
            libros.put(l.getGenero(), new java.util.TreeSet<>());
        }
        return this.libros.get(l.getGenero()).add(l);
    }
    
    public boolean removeLibro(Libro l){
        if(libros.containsKey(l.getGenero())){
            return this.libros.get(l.getGenero()).remove(l);
        }
        else{
            return false;
        }
    }
    
    private static void checkNotNullBlank(String txt){
        java.util.Objects.requireNonNull(txt);
        if(txt.isBlank()){
            throw new IllegalArgumentException();
        }
    }

    public String toString() {
        return this.nombre + " --- " + this.direccion;
    }
    
    /**
     * Obtiene todos los libros de la biblioteca
     * @return 
     */
    public Set<Libro> getLibros(){
        Set<Libro> resultado = new java.util.TreeSet<Libro>();
        for(Set<Libro> subconjunto : libros.values()){
            resultado.addAll(subconjunto);
        }
        return resultado;
    }
    
    public Set<Libro> getLibros(Genero g){
        if(libros.containsKey(g)){
            return java.util.Collections.unmodifiableSet(libros.get(g));
        }
        else{
            return new java.util.TreeSet<>();
        }
    }
    
    public int countLibrosGenero(Genero g){
        if(libros.containsKey(g)){
            return libros.get(g).size();
        }
        else{
            return 0;
        }
    }
    
    public boolean borrarLibroByIsbn(String isbn){
        for(Set<Libro> ls : libros.values()){
            Iterator<Libro> it = ls.iterator();
            while (it.hasNext()) {
                Libro next = it.next();
                if(next.getIsbn().equals(isbn)){
                    it.remove();
                    return true;
                }
            }
        }
        return false;
    }
}
