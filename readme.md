1. Crea una aplicación para gestionar una biblioteca. La biblioteca tiene una dirección, un nombre y una colección de libros. Permite dos constructores: sólo nombre (en este caso dirección = "SD") y nombre y dirección. De cada libro se guarda: ISBN, título, nombre del autor, año publicación, género (drama, comedia, policíaca, histórica, ensayo, teatro), editorial, stock, edición (>=1). Una vez creado un libro no se puede cambiar ningún dato salvo edición Haz una aplicación que al arrancar solicite el nombre de la biblioteca. Una vez insertado un nombre (sólo letras, números y espacios. No permitas cadena vacía) el sistema mostrará un menú que permitirá:

    Añadir un libro.
    Dar de baja un libro por ISBN.
    Mostrar los libros existentes (datos a mostrar: ISBN, título, autor, año).
    Mostrar número de libros por género.
    Mostrar libros por género.